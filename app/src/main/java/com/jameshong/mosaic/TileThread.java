package com.jameshong.mosaic;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by hysial on 23/07/16.
 */
public class TileThread extends Thread {

    private int mIndex;
    private String mUrl;
    private TileThreadListener mListener;

    public TileThread(int index, String url, TileThreadListener listener) {

        mIndex = index;
        mUrl = url;
        mListener = listener;
    }

    @Override
    public void run() {

        Bitmap bitmap = null;
        while (bitmap == null) {
            bitmap = downloadImageFile();
        }

        mListener.onImageFinishedDownloading(mIndex, mUrl, bitmap);
    }

    /**
     * connects to the server to download an image then return it in bitmap
     * @return downloaded image in bitmap
     */
    private Bitmap downloadImageFile() {

        URL fileUrl = null;

        try {
            fileUrl = new URL(mUrl);
        } catch(MalformedURLException e) {
            e.printStackTrace();
        }

        try {
            HttpURLConnection conn = (HttpURLConnection)fileUrl.openConnection();
            conn.setDoInput(true);
            conn.connect();
            InputStream is = conn.getInputStream();

            Bitmap bmImg = BitmapFactory.decodeStream(is);

            return bmImg;
        } catch(IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public interface TileThreadListener {

        void onImageFinishedDownloading(int index, String url, Bitmap bitmap);
    }
}
