package com.jameshong.mosaic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by hysial on 25/07/16.
 */
public class NetworkStatusReceiver extends BroadcastReceiver {

    private NetworkStatusReceiverListener mListener;
    private ConnectivityManager mConnectivityManager;

    public NetworkStatusReceiver(Context context, NetworkStatusReceiverListener listener) {

        mListener = listener;
        mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            NetworkInfo activeNetwork = mConnectivityManager.getActiveNetworkInfo();
            if (mListener != null) {
                mListener.onNetworkStatusUpdate(activeNetwork != null);
            }
        }
    }

    public interface NetworkStatusReceiverListener {

        void onNetworkStatusUpdate(boolean isConnected);
    }
}
