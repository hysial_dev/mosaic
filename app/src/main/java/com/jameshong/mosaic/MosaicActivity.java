package com.jameshong.mosaic;

import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.FrameLayout;

import com.jameshong.mosaic.fragments.GalleryFragment;
import com.jameshong.mosaic.fragments.PreviewFragment;


public class MosaicActivity extends FragmentActivity implements GalleryFragment.GalleryFragmentListener, NetworkStatusReceiver.NetworkStatusReceiverListener {

    private NetworkStatusReceiver mNetworkStatusReceiver;

    private static final String TAG_GALLERY = "TAG_GALLERY";
    private static final String TAG_PREVIEW = "TAG_PREVIEW";

    private GalleryFragment mGalleryFragment;
    private PreviewFragment mPreviewFragment;

    // UI
    FrameLayout messageContainer;
    FrameLayout fragmentContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mosaic);
        mNetworkStatusReceiver = new NetworkStatusReceiver(this, this);

        if (savedInstanceState == null) {
            fragmentContainer = (FrameLayout) findViewById(R.id.fl_fragment_container);

            mGalleryFragment = new GalleryFragment();

            getSupportFragmentManager().beginTransaction().add(R.id.fl_fragment_container, mGalleryFragment, TAG_GALLERY).commit();
        }
    }

    @Override
    protected void onResume() {

        super.onResume();
        messageContainer = (FrameLayout) findViewById(R.id.fl_message_container);
        registerReceiver(mNetworkStatusReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onPause() {

        super.onPause();
        unregisterReceiver(mNetworkStatusReceiver);
    }

    /** implementation of GalleryFragmentListener **/
    @Override
    public void onImageSelected(String path) {

        mPreviewFragment = new PreviewFragment();
        Bundle bundle = new Bundle();
        bundle.putString(mPreviewFragment.ARG_IMAGE_PATH, path);
        mPreviewFragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction()
            .replace(R.id.fl_fragment_container, mPreviewFragment, TAG_PREVIEW)
            .addToBackStack(null)
            .commit();
    }

    /** implementation of NetworkStatusReceiverListener **/
    @Override
    public void onNetworkStatusUpdate(boolean isConnected) {

        if (messageContainer != null) {
            if (isConnected) {
                messageContainer.setVisibility(View.GONE);
            } else {
                messageContainer.setVisibility(View.VISIBLE);
            }
        }
    }
}
