package com.jameshong.mosaic;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;

import java.io.File;
import java.io.IOException;

/**
 * Created by hysial on 25/07/16.
 */
public class ImageUtil {

    private static ImageUtil instance;

    private ImageUtil() {

    }

    public static ImageUtil getInstance() {

        if (instance == null) {
            instance = new ImageUtil();
        }

        return instance;
    }

    /**
     * returns the image in bitmap, but returns null if image cannot be found
     * @param imagePath: path of the image
     * @return selected image in bitmap type
     */
    public Bitmap getBitmap(String imagePath) {

        File imageFile = new File(imagePath);

        Bitmap imageBitmap = null;

        if (imageFile.exists()) {
            imageBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
        }

        return imageBitmap;
    }

    /**
     * checks if the image has been rotated then rotate the image accordingly to show it upright
     * @param imagePath: path of the image
     * @param bitmap: image in bitmap type
     * @return correctly rotated upright bitmap
     */
    public Bitmap uprightImage(String imagePath, Bitmap bitmap) {

        try {
            ExifInterface exif = new ExifInterface(imagePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            Matrix matrix = new Matrix();
            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                matrix.postRotate(90);
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                matrix.postRotate(180);
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                matrix.postRotate(270);
            }

            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            // file cannot be found in the path, so image won't be rotated
            e.printStackTrace();
        }

        return bitmap;
    }

    /**
     * resizes the image to reach the max value keeping the aspect ratio
     * @param maxWidth: desired max width of the image
     * @param maxHeight: desired max height of the image
     * @param image: image to resize
     * @param tilesize: side length of a tile in px
     * @return resized image
     */
    public Bitmap resizeImage(int maxWidth, int maxHeight, Bitmap image, int tilesize) {

        if (maxHeight > 0 && maxWidth > 0) {
            float imageRatio = (float) image.getWidth() / (float) image.getHeight();

            int scaledWidth = image.getWidth();
            int scaledHeight = image.getHeight();

            // image is wider than the display
            if (image.getWidth() > maxWidth) {
                scaledWidth = maxWidth;
                scaledHeight = (int) (scaledWidth / imageRatio);
            } else if (image.getHeight() > maxHeight) { // image is taller than the display
                scaledHeight = maxHeight;
                scaledWidth = (int) (scaledHeight * imageRatio);
            }

            scaledWidth = getClosestDivisibleNumber(scaledWidth, tilesize);
            scaledHeight = getClosestDivisibleNumber(scaledHeight, tilesize);

            image = Bitmap.createScaledBitmap(image, scaledWidth, scaledHeight, true);

            return image;
        } else {
            return image;
        }
    }

    /**
     * calculates the biggest, closest number divisible by tilesize
     * @param number: a number to be checked if it is divisible by the tile length
     * @param tilesize: side length of a tile in px
     * @return biggest, closest number divisible by tilesize
     */
    public int getClosestDivisibleNumber(int number, int tilesize) {

        // decrement until divisible by the tile size
        while (number % tilesize != 0) {
            number--;
        }

        return number;
    }

    /**
     * calculates the start index of the tile in an array
     * @param tileIndex: index of the tile
     * @param tilesize: side length of a tile in px
     * @param widthPixels: number of pixels in the width of the image
     * @return start index of the tile in an array
     */
    public int getStartIndex(int tileIndex, int tilesize, int widthPixels) {

        int nextLineTileSkip = widthPixels * tilesize;

        int numberOfTilesInRow = widthPixels / tilesize;

        int x = tileIndex % numberOfTilesInRow;
        int y = tileIndex / numberOfTilesInRow;

        int index = (x * tilesize) + (y * nextLineTileSkip);

        return index;
    }

    /**
     * calculates the average hex colour code within the tile
     * @param mPixelColours: an array of colours of pixels in the image
     * @param tileIndex: index of the tile
     * @param tilesize: side length of a tile in px
     * @param widthPixels: number of pixels in the width of the image
     * @return average hex colour code
     */
    public int getAverageColour(int[] mPixelColours, int tileIndex, int tilesize, int widthPixels) {

        int startIndex = getStartIndex(tileIndex, tilesize, widthPixels);
        int colour;
        int R = 0;
        int G = 0;
        int B = 0;
        int total = tilesize * tilesize;

        for (int j = 0; j < tilesize; j++) {
            for (int i = 0; i < tilesize; i++) {
                colour = mPixelColours[startIndex + i + (widthPixels * j)];
                R += Color.red(colour);
                G += Color.green(colour);
                B += Color.blue(colour);
            }
        }

        return Color.rgb(R / total, G / total, B / total);
    }
}
