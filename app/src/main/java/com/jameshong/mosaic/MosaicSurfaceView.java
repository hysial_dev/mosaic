package com.jameshong.mosaic;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

/**
 * Created by hysial on 23/07/16.
 */
public class MosaicSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    private static String SERVER_URL;
    private static Context mContext;
    private static int mDeviceWidth = 800;
    private static int mDeviceHeight = 800;

    private int mScaledWidth;
    private int mScaledHeight;
    private int[] mPixelColours;

    private TileThreadManager mTileThreadManager;
    private TileDistributorThread mTileDividerThread;

    private String mImagePath;
    private Bitmap mImageBitmap;

    public MosaicSurfaceView(Context context, String path) {

        super(context);
        mContext = context;
        mImagePath = path;
        init();
    }

    public MosaicSurfaceView(Context context, AttributeSet attrs) {

        super(context, attrs);
        init();
    }

    public MosaicSurfaceView(Context context, AttributeSet attrs, int defStyle) {

        super(context, attrs, defStyle);
        init();
    }

    private void init() {

        getHolder().addCallback(this);
        SERVER_URL = mContext.getString(R.string.api_url);
        mTileDividerThread = new TileDistributorThread();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        mDeviceWidth = View.MeasureSpec.getSize(widthMeasureSpec);
        mDeviceHeight = View.MeasureSpec.getSize(heightMeasureSpec);

        setMeasuredDimension(mDeviceWidth, mDeviceHeight);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        prepareImage();

        mTileThreadManager = TileThreadManager.getInstance();
        mTileThreadManager.init(this, mScaledWidth/Constants.TILE_SIZE, mScaledHeight/Constants.TILE_SIZE);

        // check if thread is already running (user came back from home button)
        try {
            mTileDividerThread.setRunning(true);
            mTileDividerThread.start();
        } catch (IllegalThreadStateException e) {
            e.printStackTrace();
            mTileDividerThread = new TileDistributorThread();
            mTileDividerThread.setRunning(true);
            mTileDividerThread.start();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

        boolean retry = true;
        mTileDividerThread.setRunning(false);
        while (retry) {
            try {
                mTileDividerThread.join();
                mTileThreadManager.stopDownloadTiles();
                retry = false;
            } catch (InterruptedException e) {

            }
        }
    }


    /**
     * runs all methods to calculate image related values
     */
    private void prepareImage() {

        mImageBitmap = ImageUtil.getInstance().getBitmap(mImagePath);

        if (mImageBitmap != null) {
            mImageBitmap = ImageUtil.getInstance().uprightImage(mImagePath, mImageBitmap);
            mImageBitmap = ImageUtil.getInstance().resizeImage(mDeviceWidth, mDeviceHeight, mImageBitmap, Constants.TILE_SIZE);
            mScaledWidth = mImageBitmap.getWidth();
            mScaledHeight = mImageBitmap.getHeight();

            mPixelColours = new int[mScaledWidth * mScaledHeight];
            mImageBitmap.getPixels(mPixelColours, 0, mScaledWidth, 0, 0, mScaledWidth, mScaledHeight);
        }
    }

    /**
     * draws bitmap on canvas at a given y position
     * @param image: bitmap to draw
     * @param y: y position of image
     */
    public void drawOnCanvas(Bitmap image, int y) {

        Canvas canvas = getHolder().lockCanvas(null);
        y *= Constants.TILE_SIZE;

        if (canvas != null) {
            canvas.drawBitmap(image, getCenteredImageX(), y + getCenteredImageY(), null);
            getHolder().unlockCanvasAndPost(canvas);
        }
    }

    /**
     * calculates the x position of centered image
     * @return the x position of centered image
     */
    private int getCenteredImageX() {

        return (mDeviceWidth - mScaledWidth)/2;
    }

    /**
     * calculates the y position of centered image
     * @return the y position of centered image
     */
    private int getCenteredImageY() {

        return (mDeviceHeight - mScaledHeight)/2;
    }


    /** Thread to divide the image into tiles then assign it to Tile Thread Manager **/
    public class TileDistributorThread extends Thread {

        private boolean myThreadRun = false;

        public void setRunning(boolean b) {

            myThreadRun = b;
        }

        @Override
        public void run() {

            // divide the image into tiles
            int x = 0;
            int y = 0;
            int i = 0; // tile index

            while (y < mScaledHeight) {
                while (x < mScaledWidth) {
                    if (!myThreadRun) {
                        continue;
                    }

                    // calculate average colour in a tile
                    int averageColour = ImageUtil.getInstance().getAverageColour(mPixelColours, i, Constants.TILE_SIZE, mScaledWidth);
                    String hexColour = String.format("%06X", (0xFFFFFF & averageColour));

                    // ask the manager to download the tile image
                    mTileThreadManager.startDownloadTile(i, String.format(SERVER_URL, Constants.TILE_SIZE, Constants.TILE_SIZE, hexColour));
                    x += Constants.TILE_SIZE;
                    i++;
                }
                x = 0;
                y += Constants.TILE_SIZE;
            }
        }
    }
}
