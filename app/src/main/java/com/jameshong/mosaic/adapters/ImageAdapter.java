package com.jameshong.mosaic.adapters;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.jameshong.mosaic.R;
import com.squareup.picasso.Picasso;

import java.io.File;

/**
 * Created by hysial on 22/07/16.
 */
public class ImageAdapter extends BaseAdapter {

    private Context mContext;

    private String[] mImagePaths;

    public ImageAdapter(Context context) {

        mContext = context;

        final String orderBy = MediaStore.Images.Media.DATE_MODIFIED + " DESC"; // ASC to show oldest photo first | DESC to show newest photo first

        Cursor imageCursor = mContext.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, null, null, orderBy);
        int dataColumnIndex = imageCursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        int count = imageCursor.getCount();
        mImagePaths = new String[count];

        imageCursor.moveToFirst();
        while(!imageCursor.isAfterLast()) {
            mImagePaths[imageCursor.getPosition()] = imageCursor.getString(dataColumnIndex);
            imageCursor.moveToNext();
        }
    }

    @Override
    public int getCount() {

        return mImagePaths.length;
    }

    @Override
    public Object getItem(int position) {

        return mImagePaths[position];
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ImageView imgThumb;

        if (convertView == null) {
            imgThumb = (ImageView)LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.lsv_item_gallery, parent, false);
            /*
            imgThumb.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    int position = (int)v.getTag();
//                    Toast.makeText(v.getContext(), "mImagePaths: " + mImagePaths[position], Toast.LENGTH_LONG).show();
                }
            });
            */
        } else {
            imgThumb = (ImageView) convertView;
        }

        imgThumb.setTag(position);

        // much smoother with Picasso caching
        Picasso.with(mContext).load(
                new File(mImagePaths[position])).centerCrop().resize(240, 240)
                .into(imgThumb);

        return imgThumb;
    }
}
