package com.jameshong.mosaic;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by hysial on 23/07/16.
 */
public class TileThreadManager implements TileThread.TileThreadListener {

    private static TileThreadManager instance;
    private static ThreadPoolExecutor mThreadPoolExecutor;
    private static BlockingQueue<Runnable> mWorkQueue;
    private static final int KEEP_ALIVE_TIME = 1;
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;
    private static final int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();

    private static MosaicSurfaceView mSurfaceView;
    private static ConcurrentHashMap<String, Bitmap> mImageCache;
    private static String[][] mImageKeys;

    private static int mCurrentRow = 0;

    private TileThreadManager() {

        mWorkQueue = new LinkedBlockingQueue<Runnable>();
        mImageCache = new ConcurrentHashMap<String, Bitmap>();
    }

    public static synchronized TileThreadManager getInstance() {

        if (instance == null) {
            instance = new TileThreadManager();
        }

        return instance;
    }

    /**
     * initialise values
     * @param surfaceView: surface view reference
     * @param numberOfTileWidth: number of tiles in the width of image
     * @param numberOfTileHeight: number of tiles in the height of image
     */
    public void init(MosaicSurfaceView surfaceView, int numberOfTileWidth, int numberOfTileHeight) {

        mSurfaceView = surfaceView;
        // best number of threads is found to be (# of cores * 2) + 1
        mThreadPoolExecutor = new ThreadPoolExecutor(
                NUMBER_OF_CORES*2 + 1,
                NUMBER_OF_CORES*2 + 1,
                KEEP_ALIVE_TIME,
                KEEP_ALIVE_TIME_UNIT,
                mWorkQueue);

        // reset values
        mCurrentRow = 0;
        mWorkQueue.clear();
        mImageCache.clear();
        mImageKeys = new String[numberOfTileHeight][numberOfTileWidth];
    }

    /**
     * creates a thread to download a required tile image
     * @param index: tile index
     * @param url: url of the tile image
     */
    public void startDownloadTile(int index, String url) {

        if (mImageCache.containsKey(url)) {
            saveKey(index, url);
            checkRowAndDraw();
        } else {
            Thread tileThread = new TileThread(index, url, instance);
            instance.mThreadPoolExecutor.execute(tileThread);
        }
    }

    /**
     * stops all threads in the pool
     */
    public void stopDownloadTiles() {

        mThreadPoolExecutor.shutdownNow();
    }

    /**
     * saves the key (url) so that we know the tile has an image downloaded
     * @param index: tile index
     * @param key: url of the tile image
     */
    private synchronized void saveKey(int index, String key) {

        int x = index % mImageKeys[0].length;
        int y = index / mImageKeys[0].length;

        mImageKeys[y][x] = key;
    }

    /**
     * checkes if the current row has all the tile images downloaded
     * @return boolean that tells if the current row has all tile images downloaded
     */
    private synchronized boolean isCurrentRowReady() {

        try {
            for (int i = 0; i < mImageKeys[mCurrentRow].length; i++) {
                if (mImageKeys[mCurrentRow][i] == null) {
                    return false;
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * creates a bitmap that combines all tile images in the current row
     * @return bitmap that combines all tile images in the current row
     */
    private synchronized Bitmap getCurrentRowBitmap() {

        ArrayList<Bitmap> bitmapsInRow = new ArrayList<Bitmap>();

        for (int i = 0; i < mImageKeys[mCurrentRow].length; i++) {
            bitmapsInRow.add(mImageCache.get(mImageKeys[mCurrentRow][i]));
        }

        Bitmap combinedBitmap = combineTilesHorizontallyIntoBitmap(bitmapsInRow);

        return combinedBitmap;
    }

    /**
     * checks if current row is ready
     * if so then draw
     */
    private synchronized void checkRowAndDraw() {

        if (isCurrentRowReady()) {
            Bitmap bitmap = getCurrentRowBitmap();
            // TODO: find out the reason why it needs to draw multiple times
            for (int i = 0; i < 3; i++) {
                mSurfaceView.drawOnCanvas(bitmap, mCurrentRow);
            }

            mCurrentRow++;
        }
    }

    /**
     * combines list of bitmaps horizontally into a single bitmap
     * @param bitmaps: list of bitmaps to combine
     * @return horizontally combined bitmap
     */
    private Bitmap combineTilesHorizontallyIntoBitmap(ArrayList<Bitmap> bitmaps) {

        int w = 0;
        int h = 0;
        // calculate the width and height of combined bitmap
        for (int i = 0; i < bitmaps.size(); i++) {
            if (i < bitmaps.size() - 1) {
                h = bitmaps.get(i).getHeight() > bitmaps.get(i + 1).getHeight() ? bitmaps.get(i).getHeight() : bitmaps.get(i + 1).getHeight();
            }
            w += bitmaps.get(i).getWidth();
        }

        Bitmap temp = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(temp);
        int x = 0;
        for (int i = 0; i < bitmaps.size(); i++) {
            x = (i == 0 ? 0 : x + bitmaps.get(i).getWidth());
            canvas.drawBitmap(bitmaps.get(i), x, 0f, null);
        }

        return temp;
    }

    /**
     * combines list of bitmaps horizontally into a single bitmap
     * @param bitmaps: list of bitmaps to combine
     * @return vertically combined bitmap
     */
    private Bitmap combineTilesVerticallyIntoBitmap(ArrayList<Bitmap> bitmaps) {

        int w = 0;
        int h = 0;
        // calculate the width and height of combined bitmap
        for (int i = 0; i < bitmaps.size(); i++) {
            if (i < bitmaps.size() - 1) {
                w = bitmaps.get(i).getWidth() > bitmaps.get(i + 1).getWidth() ? bitmaps.get(i).getWidth() : bitmaps.get(i + 1).getWidth();
            }
            h += bitmaps.get(i).getHeight();
        }

        Bitmap temp = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(temp);
        int y = 0;
        for (int i = 0; i < bitmaps.size(); i++) {
            y = (i == 0 ? 0 : y + bitmaps.get(i).getHeight());
            canvas.drawBitmap(bitmaps.get(i), 0f, y, null);
        }

        return temp;
    }

    /** implementation of TileThreadListener **/
    /**
     * add the returned image in the image cache and see if current row has completed downloading tiles
     * if thread happens to fail downloading then retry
     * @param index: tile index
     * @param url: url to download a tile image
     * @param bitmap: tile image received from the server
     */
    @Override
    public void onImageFinishedDownloading(int index, String url, Bitmap bitmap) {

        if (bitmap == null) {
            TileThread retryThread = new TileThread(index, url, this);
            mThreadPoolExecutor.execute(retryThread);
        } else {
            mImageCache.put(url, bitmap);
            saveKey(index, url);
            checkRowAndDraw();
        }
    }
}
