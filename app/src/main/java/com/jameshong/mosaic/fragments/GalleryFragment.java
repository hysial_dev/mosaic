package com.jameshong.mosaic.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.jameshong.mosaic.R;
import com.jameshong.mosaic.adapters.ImageAdapter;


public class GalleryFragment extends Fragment {

    private GalleryFragmentListener mListener;
    private ImageAdapter mImageAdapter;

    // UI
    private GridView mGalleryGridView;

    public GalleryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_gallery, container, false);

        mGalleryGridView = (GridView) rootView.findViewById(R.id.gv_image_gallery);
        mGalleryGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                String path = (String) mImageAdapter.getItem(position);
                onGalleryImageSelected(path);
            }
        });

        mImageAdapter = new ImageAdapter(getContext());
        mGalleryGridView.setAdapter(mImageAdapter);

        return rootView;
    }

    public void onGalleryImageSelected(String path) {

        if (mListener != null) {
            mListener.onImageSelected(path);
        }
    }

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);

        if (context instanceof GalleryFragmentListener) {
            mListener = (GalleryFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement GalleryFragmentListener");
        }
    }

    @Override
    public void onDetach() {

        super.onDetach();
        mListener = null;
    }

    public interface GalleryFragmentListener {

        void onImageSelected(String path);
    }
}
