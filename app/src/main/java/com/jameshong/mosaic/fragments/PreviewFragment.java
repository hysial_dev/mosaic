package com.jameshong.mosaic.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jameshong.mosaic.MosaicSurfaceView;


public class PreviewFragment extends Fragment {

    public static final String ARG_IMAGE_PATH = "IMAGE_PATH";
    private String mImagePath;

    // UI
    MosaicSurfaceView mMosaicSurfaceView;

    public PreviewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mImagePath = getArguments().getString(ARG_IMAGE_PATH);
        mMosaicSurfaceView = new MosaicSurfaceView(getContext(), mImagePath);

        return mMosaicSurfaceView;
    }
}