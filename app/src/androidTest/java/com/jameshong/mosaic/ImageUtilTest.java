package com.jameshong.mosaic;

import android.graphics.Bitmap;
import android.graphics.Color;

import junit.framework.TestCase;


/**
 * Created by hysial on 25/07/16.
 */
public class ImageUtilTest extends TestCase {

    public void testGettingBitmapWithWrongPath() {

        ImageUtil imageUtil = ImageUtil.getInstance();
        assertEquals(null, imageUtil.getBitmap("/com.jameshong.mosaic/ok/abc123.png"));
    }

    public void testResizingImageLandscapeExceedWidth() {

        ImageUtil imageUtil = ImageUtil.getInstance();
        Bitmap bitmap = Bitmap.createBitmap(2000, 800, Bitmap.Config.ARGB_8888);
        Bitmap actualImage = imageUtil.resizeImage(800, 1600, bitmap, 40);

        assertEquals(800, actualImage.getWidth());
        assertEquals(320, actualImage.getHeight());
    }

    public void testResizingImageLandscapeMatchingWidth() {

        ImageUtil imageUtil = ImageUtil.getInstance();
        Bitmap bitmap = Bitmap.createBitmap(800, 400, Bitmap.Config.ARGB_8888);
        Bitmap actualImage = imageUtil.resizeImage(800, 1600, bitmap, 40);

        assertEquals(800, actualImage.getWidth());
        assertEquals(400, actualImage.getHeight());
    }

    public void testResizingImageLandscapeSmallerWidth() {

        ImageUtil imageUtil = ImageUtil.getInstance();
        Bitmap bitmap = Bitmap.createBitmap(600, 400, Bitmap.Config.ARGB_8888);
        Bitmap actualImage = imageUtil.resizeImage(800, 1600, bitmap, 40);

        assertEquals(600, actualImage.getWidth());
        assertEquals(400, actualImage.getHeight());
    }

    public void testResizingImagePortraitExceedHeight() {

        ImageUtil imageUtil = ImageUtil.getInstance();
        Bitmap bitmap = Bitmap.createBitmap(500, 2000, Bitmap.Config.ARGB_8888);
        Bitmap actualImage = imageUtil.resizeImage(800, 1600, bitmap, 40);

        assertEquals(400, actualImage.getWidth());
        assertEquals(1600, actualImage.getHeight());
    }
    public void testResizingImageLandscapeMatchingHeight() {

        ImageUtil imageUtil = ImageUtil.getInstance();
        Bitmap bitmap = Bitmap.createBitmap(400, 1600, Bitmap.Config.ARGB_8888);
        Bitmap actualImage = imageUtil.resizeImage(800, 1600, bitmap, 40);

        assertEquals(400, actualImage.getWidth());
        assertEquals(1600, actualImage.getHeight());
    }

    public void testResizingImageLandscapeSmallerHeight() {

        ImageUtil imageUtil = ImageUtil.getInstance();
        Bitmap bitmap = Bitmap.createBitmap(600, 1200, Bitmap.Config.ARGB_8888);
        Bitmap actualImage = imageUtil.resizeImage(800, 1600, bitmap, 40);

        assertEquals(600, actualImage.getWidth());
        assertEquals(1200, actualImage.getHeight());
    }

    public void testResizingImageWithWrongMax() {

        ImageUtil imageUtil = ImageUtil.getInstance();
        Bitmap bitmap = Bitmap.createBitmap(600, 1200, Bitmap.Config.ARGB_8888);
        Bitmap actualImage = imageUtil.resizeImage(0, -600, bitmap, 40);

        assertEquals(600, actualImage.getWidth());
        assertEquals(1200, actualImage.getHeight());
    }

    public void testCalculatingClosestDivisibleNumber() {

        ImageUtil imageUtil = ImageUtil.getInstance();
        assertEquals(320, imageUtil.getClosestDivisibleNumber(325, 32));
    }

    public void testGettingStartIndexInFirstRow() {

        ImageUtil imageUtil = ImageUtil.getInstance();
        assertEquals(10, imageUtil.getStartIndex(2, 5, 15));
    }

    public void testGettingStartIndexInSecondRow() {

        ImageUtil imageUtil = ImageUtil.getInstance();
        assertEquals(80, imageUtil.getStartIndex(4, 5, 15));
    }

    public void testGettingAverageColourWithSameColours() {

        ImageUtil imageUtil = ImageUtil.getInstance();

        int size = 150;
        int[] pixels = new int[size];

        for (int i = 0; i < size; i++) {
            pixels[i] = Color.RED;
        }

        assertEquals(Color.RED, imageUtil.getAverageColour(pixels, 1, 5, 15));
    }
}
