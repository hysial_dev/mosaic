# README #

## Purpose ##
##### This project is to implement the following features: #####
1. A user selects a local image file.
2. The app must:
   * load that image;
   * divide the image into tiles;
   * find a representative color for each tile (average);
   * fetch a tile from the provided server (see below) for that color;
   * composite the results into a photomosaic of the original image;
3. The composited photomosaic should be displayed according to the following
   constraints:
   * tiles should be rendered a complete row at a time (a user should never
      see a row with some completed tiles and some incomplete)
   * the mosaic should be rendered from the top row to the bottom row.
4. The client app should make effective use of parallelism and asynchrony.

## Config ##
* URL can be modified in strings.xml
* Tile size can be modified in Constants.java

## Third party library used ##
* Picasso: to cache images for gallery view


## Test cases ##
##### Test cases are written to test methods related to image #####
1. test getting a bitmap when wrong path of image is given
2. test resizing a landscape image that exceeds screen width
3. test resizing a landscape image that matches screen width
4. test resizing a landscape image that does not exceed screen width
5. test resizing a portrait image that exceeds screen height
6. test resizing a portrait image that matches screen height
7. test resizing a portrait image that does not exceed screen height
8. test resizing an image when wrong screen width, height are given
9. test calculating a number that is largest (yet smaller than the given number) and divisible by a specific number
10. test getting the start index of a given tile that is in the first row
11. test getting the start index of a given tile that is in the second row
12. test getting the average colour when a tile has all the same colours